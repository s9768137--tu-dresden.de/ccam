% CNTFET compact model

function Qs_cnt=ccam_new_Qs(Vd,Vg,Vs,PARA)

phit = 8.617e-5*300;   % Thermal voltage [V]

scalingFactor = 1e6;

% type modifier
devtype    = PARA.devtype;

% structural parmaeters
w       = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens 	= PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst     = PARA.pst; 

%S-Charge Parameters

ctn0 = PARA.ctn0; 
mqs  = PARA.mqs; 
vthqs  = PARA.vthqs;
pqsd   = PARA.pqsd; 


Vgs = Vg - Vs;

% /////   Charge  model    ///////////////////////////////////////

vg_t   = Vgs-vthqs;
		
nom    = 1+exp(    vg_t /mqs/phit);
denom  = 1+exp(  -vthqs /mqs/phit);
fqsn   = mqs*phit*log( nom / denom );
if devtype == 1
    Qs_cnt = devtype*pqsd*w*scalingFactor*pst*dens*(ctn0*fqsn);
else
    Qs_cnt = devtype*pqsd*w*scalingFactor*pst*dens*(ctp0*fqsn);
end

end



