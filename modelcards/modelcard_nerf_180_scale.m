function [PARA] = modelcard_nerf_180_scale(wg,ngf,nta,pmt)

% wg finger gate width [um]
% ngf number of gate fingers
% nta tube density [1/um]
% pmt metallic tube fraction

wgf  = wg*ngf;
nsem = (1-pmt)*wgf*nta;
nmet = (  pmt)*wgf*nta;

% sem. tubes
PARA.currmod   = 2;
% sem. tubes - n
PARA.idstn     = 2.098e-07   *nsem;
PARA.pin       = 5/8.1;
PARA.m1in      = 2.5;
PARA.m2in      = 8;
PARA.vthin     = 0.3;
PARA.dvthin    = 0.1;
PARA.vth0in    = 19.9292;
PARA.athin     = 12.0615;
PARA.sthin     = 0.4889;
% sem. tubes - p
PARA.idstp     = 0   *nsem;
PARA.pip       = 0;
PARA.m1ip      = 1;
PARA.m2ip      = 1;
PARA.vthip     = -0.0300;
PARA.dvthip    = 0.6410;
PARA.vth0ip    = 5; % not used
PARA.athip     = 2; % not used
PARA.sthip     = 0.2000; % not used
% sem. tubes vds dependence
PARA.betds     = 0.5000;
PARA.vdcr      = 0.1500;
PARA.betdcr    = 2.2538;
PARA.smss      = 10;
PARA.facss     = 1;
% contact resistance
if (nsem>0)
PARA.rscs     = 8e3      /nsem;
PARA.rdcs     = 8e3      /nsem;
else
PARA.rscs     = 0e3;
PARA.rdcs     = 0e3;    
end
% charge modelling
PARA.chargemod = 2;
PARA.ctn0      = 1.2997e-17   *nsem;
PARA.ctp0      = 0            *nsem;
PARA.mqs       = 3.7645;
PARA.vthqs     = 0.2563;
PARA.vth0qs    = 21.05; % not used
PARA.athqs     = 45; % not used
PARA.mqd       = 8;
PARA.vthqd     = 0.4;
PARA.vth0qd    = 50;% not used
PARA.athqd     = 50;% not used
PARA.pqsd      = 0.3508;
% metallic tubes
if (nmet>0)
PARA.rmta     = 20e3      /nmet;
PARA.amto     = 50e3      /nmet;
else
PARA.rmta     = 0;
PARA.amto     = 50e3;
end
PARA.cmt      = 20e-18    *nmet;
PARA.pqmt     = 0.5;
if (nmet>0) 
PARA.rscm     = 10e+03    /nmet;
PARA.rdcm     = 10e+03    /nmet;
else
PARA.rscm     = 10e+03;
PARA.rdcm     = 10e+03;    
end
% finger/gate resistance
PARA.rsf      = 1e-3;
PARA.rdf      = 1e-3;
PARA.rg       = (1.617E-006+5.321E-05/wg+0.8483e-6*wg)/(180e-9*ngf);
% parasitic capacitacne
PARA.cgspar1  = 0;
PARA.cgdpar1  = 0;
PARA.cgdpar2  = 375.1e-18    *wgf;
PARA.cgspar2  = 375.1e-18    *wgf;
PARA.cdspar   = 1125e-18     *wgf;
% temperatur dependence
PARA.tnom     = 26.8500;
PARA.rths     = 1;
PARA.rthm     = 1;
PARA.zetrmta  = 0;
PARA.zetamto  = 0;
PARA.alvthin  = 0;
PARA.alvth0in = 0;
PARA.alvthip  = 0;
PARA.alvth0ip = 0;
PARA.alidst   = 0;
PARA.alct0    = 0;
PARA.alvthqs  = 0;
PARA.alvth0qs = 0;
PARA.alvthqd  = 0;
PARA.alvth0qd = 0;
PARA.alrscon  = 0;
PARA.alrmcon  = 0;
% noise modeling
PARA.hoogef   = 0.0020;
PARA.beta_fn  = 1;
PARA.fanof    = 1;
PARA.fanofmt  = 1;
% trap modeling
PARA.atrap    = 0.5000;
PARA.btrap    = 0.5000;
PARA.ctrap    = 2;
PARA.w0trap   = 0;
PARA.strap    = 0;