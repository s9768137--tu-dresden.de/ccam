function gmt = dIMdVd_scat(biasdata,PARA)


deltaV=0.001;

vgsdata=biasdata(:,1);
vdsdata=biasdata(:,2);

biasdata_vds1=vdsdata-deltaV/2;
biasdata_vds2=vdsdata+deltaV/2;

[Imt_vds1]=IM_avmt0_scat([vgsdata,biasdata_vds1],PARA);
[Imt_vds2]=IM_avmt0_scat([vgsdata,biasdata_vds2],PARA);

gmt = (Imt_vds2-Imt_vds1)/deltaV;

end