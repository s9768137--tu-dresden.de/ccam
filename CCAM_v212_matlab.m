function [Id,Qs,Qd,YP,SP,ZP,Vgisi,Vdisi,Vdmsm] = CCAM_v212_matlab(PARA,Vg,Vs,Vd,f)

% initialization
Id      = zeros(length(Vg),length(Vd));
Vgisi   = zeros(length(Vg),length(Vd)); 
Vdisi   = zeros(length(Vg),length(Vd)); 
Vdmsm   = zeros(length(Vg),length(Vd));
Qs      = zeros(length(Vg),length(Vd)); 
Qd      = zeros(length(Vg),length(Vd)); 

if ~isfield(PARA,'dt'); PARA.dt=0; end;
if nargin < 5; f=0; end;

% Iterate over each Vg and Vd
for k = 1:length(Vg)
    for l = 1: length(Vd)

        [Id(k,l),Vgisi(k,l),Vdisi(k,l),Vdmsm(k,l),INTERN]=solve_internal_model(Vg(k),Vs,Vd(l),PARA);

        Qs(k,l)     = INTERN.Qs;
        Qd(k,l)     = INTERN.Qd;
    end
end

if max(f)<=0
    YP{1}.freq  = 0;
    YP{1}.Y11   = 0;
    YP{1}.Y12   = 0;
    YP{1}.Y21   = 0;
    YP{1}.Y22   = 0;
    
    ZP{1}.freq  = 0;
    ZP{1}.Z11   = 0;
    ZP{1}.Z12   = 0;
    ZP{1}.Z21   = 0;
    ZP{1}.Z22   = 0;
    
    SP{1}.freq  = 0;
    SP{1}.S11   = 0;
    SP{1}.S12   = 0;
    SP{1}.S21   = 0;
    SP{1}.S22   = 0;
else
    % calc Y-Parameters
    for k = 1:length(Vg)
        for l = 1:length(Vd)
            for freqIndex = 1:length(f)
                
                V.Vgisi = Vgisi(k,l);
                V.Vdisi = Vdisi(k,l);
                V.Vdmsm = Vdmsm(k,l);
                [Y11,Y12,Y21,Y22,Z11,Z12,Z21,Z22,S11,S12,S21,S22] = YPARAM(f(freqIndex),V,PARA);
                
                YP{freqIndex}.freq(1,l) = f(freqIndex);
                YP{freqIndex}.Y11(k,l) = Y11;
                YP{freqIndex}.Y12(k,l) = Y12;
                YP{freqIndex}.Y21(k,l) = Y21;
                YP{freqIndex}.Y22(k,l) = Y22;
                
                ZP{freqIndex}.freq(1,l) = f(freqIndex);
                ZP{freqIndex}.Z11(k,l) = Z11;
                ZP{freqIndex}.Z12(k,l) = Z12;
                ZP{freqIndex}.Z21(k,l) = Z21;
                ZP{freqIndex}.Z22(k,l) = Z22;
                
                SP{freqIndex}.freq(1,l) = f(freqIndex);
                SP{freqIndex}.S11(k,l) = S11;
                SP{freqIndex}.S12(k,l) = S12;
                SP{freqIndex}.S21(k,l) = S21;
                SP{freqIndex}.S22(k,l) = S22;
            end
        end
    end
end

end

function [Id,Vgisi,Vdisi,Vdmsm,INTERN] = solve_internal_model(Vg,Vs,Vd,PARA)

% Intialize
INTERN  = [];

% Bias data
Vgs     = Vg - Vs;
Vds     = Vd - Vs;

% Model
options     = optimset('DiffMinChange',1e-3,'DiffMaxChange',1e1,'display','off','tolfun',1e-6);
Vext        = [Vgs, Vds];

% NEWTON for contact resistance
V       = fsolve(@(Vint) F_IT(Vint,Vext,PARA,1),Vext,options);
% V=Vext;
OUT     = F_IT(V,Vext,PARA,0);
Id      = OUT.ID;
Vgisi   = OUT.Vgisi;
Vdisi   = OUT.Vdisi;
Vdmsm   = OUT.Vdmsm;

[INTERN.Qs,INTERN.Qd] = charge_emp(Vgisi,Vdisi,PARA);

end

function [F]=F_IT(Vint,Vext,PARA,flag)

% structural parmeters
w            = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens         = PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst          = PARA.pst;            % from (0:1];	    // semiconducting CNTs to total CNTs ratio - [-]
scalingFactor = 1e6;

% Contact and finger resistance parameters
Rsc     = PARA.rscs;
Rdc     = PARA.rdcs;
Rsf     = PARA.rsf;
Rdf     = PARA.rdf;

% separate Vgs, Vds
Vgisi = Vint(1);
Vdisi = Vint(2);

% different models
ISEM    = IS_emp(Vgisi,Vdisi,PARA);

% voltage drop contact resistance SEM
Vdxsx   = Vdisi + ISEM*(Rsc+Rdc)/(w*scalingFactor*pst*dens);
Vgsx    = Vgisi + ISEM*(Rsc)/(w*scalingFactor*pst*dens);

% metallic tubes
if PARA.rmta>0
    IMET = IM_avmt0_scat(Vdxsx,PARA);
else
    IMET = 0;
end

% voltage drop finger resistance
Vgs     = Vgsx  + (ISEM+IMET)*Rsf*w*scalingFactor;
Vds     = Vdxsx + (ISEM+IMET)*(Rsf+Rdf)*w*scalingFactor;

if any(imag(Vgs))
    disp('Strom ist komplex') 
end

% save
if flag %% for newton
    Vsolve  = [Vgs, Vds];
    F       = Vext - Vsolve;
else
    Rsm     = PARA.rscm;
    Rdm     = PARA.rdcm;
    F.ID    = ISEM + IMET;
    F.Vdmsm = Vdxsx - IMET*(Rsm+Rdm)/(w*scalingFactor*(1-pst)*dens);
    F.Vdisi = Vdisi;
    F.Vgisi = Vgisi;
end

end


% /////   Current model    ///////////////////////////////////////
function IS = IS_emp(Vgs,Vds,PARA)

if (PARA.currmod==1)
    IS  = CCAM_MATLAB_IDxVG_NANOHUB(Vgs,Vds,PARA);
else
    IS  = id_flat(Vgs,Vds,PARA);
end    

end

% currmod = 1
function curr=CCAM_MATLAB_IDxVG_NANOHUB(Vgs,Vds,PARA)

% constant
Vt          = 0.0259;       % Thermal voltage [V]

% structural parmeters
w            = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens         = PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst          = PARA.pst;            % from (0:1];	    // semiconducting CNTs to total CNTs ratio - [-]
scalingFactor = 1e6;

% DC parameters s-tubes
idstn   = PARA.idstn;       % from [0:10]       % current factor n-type conduction
vthin   = PARA.vthin;       % from [-10:10];    % threshold voltage n-type
vth0in  = PARA.vth0in;      % from (0:100];     % fit parameter n-type current
athin   = PARA.athin;       % from (0:100];     % fit parameter n-type current
sthin   = PARA.sthin;       % from [0:1];       % fit parameter for n-type subthreshold slope
idstp   = PARA.idstp;       % from [0:10];      % current factor p-type conduction
vthip   = PARA.vthip;       % from [-10:10];    % threshold voltage p-type
vth0ip  = PARA.vth0ip;      % from (0:100];     % fit parameter p-type current
athip   = PARA.athip;       % from (0:100];     % fit parameter p-type current
sthip   = PARA.sthip;       % from [0:1];       % fit parameter for subthreshold slope
vdcr    = PARA.vdcr;        % from (0:50];      % fit parameter for current calculation
betdcr  = PARA.betdcr;      % from (0:50];      % fit parameter for current calculation
smss    = PARA.smss;        % from [0:100];     % fit parameter transition to subthreshold region
facss   = PARA.facss;       % from [1:10];      % fit parameter DIBL

% parameters for temperature dependence
dt          = PARA.dt;
alvthin     = PARA.alvthin;
alvth0in    = PARA.alvth0in;
alvthip     = PARA.alvthip;
alvth0ip    = PARA.alvth0ip;
alidst      = PARA.alidst;

% temperature dependent parameter modification
vth0in_t    = vth0in*(1+alvth0in*dt);
vthin_t     = vthin*(1+alvthin*dt);
vth0ip_t    = vth0ip*(1+alvth0ip*dt);
vthip_t     = vthip*(1+alvthip*dt);
idstn_t     = idstn*(1+alidst*dt);
idstp_t     = idstp*(1+alidst*dt);

% DIBL modeling
if facss < 1
    a_ss=1;
    b_ss=2/(1-facss)-1;
    c_ss=2/(1-facss);
elseif facss == 1
    a_ss=0;
    b_ss=1;
    c_ss=1;
else 
    a_ss=-1;
    b_ss=(1+facss)/(facss-1);
    c_ss=2/(facss-1);
end

% Bias data
Vgd     = Vgs - Vds;

% model
% //electron current   fgsn
vg_eff_n        = Vgs-0.5*(Vds - hypot(Vds, Vt));
vg_tn           = ((vg_eff_n-vthin_t)+hypot(vg_eff_n-vthin_t,sthin))/2;
athn_con        = (log(1+exp(1./athin)));   
un              = 1-(vth0in_t./vg_tn);
test            = un/athin;
fgsn            = zeros(length(test),1); 
fgsn(test>-10)  = log(1+exp(un(test>-10)/athin))/athn_con;
fgsn(test<=-10) = exp(un(test<=-10)/athin)/athn_con;

% //hole current   fgsp
vg_eff_p            = Vgd+0.5*(Vds - hypot(Vds, Vt));
vg_tp               = ((-vg_eff_p+vthip_t)+hypot(-vg_eff_p+vthip_t,sthip))/2;
athp_con            = (log(1+exp(1./athip)));  
up                  = 1-(vth0ip_t./vg_tp);
test2               = up/athip;
fgsp                = zeros(length(test2),1); 
fgsp(test2 > -10)   = log(1+exp(up(test2 > -10)/athip))/athp_con;
fgsp(test2<=-10)    = exp(up(test2<=-10)/athip)/athp_con;

fgs     = idstp_t*fgsp + idstn_t*fgsn;

% //current  fds
if idstp == 0 
    u   = Vds./((tanh(a_ss*smss*(vg_eff_n-vthin_t)) + b_ss)/c_ss*vdcr);
elseif idstn == 0
    u   = Vds./((tanh(a_ss*smss*(-vg_eff_p+vthip_t)) + b_ss)/c_ss*vdcr);
else
    u   = Vds./vdcr;
end

fds     = u./power(1 + power(hypot(u,Vt/vdcr),betdcr),(1/betdcr));

% result
curr    = w*scalingFactor*pst*dens*fgs.*fds;

end

% currmod = 2
function curr = id_flat(Vgs,Vds,PARA)

% constant
Vt          = 0.0259;       % Thermal voltage [V]

% structural parmeters
w            = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens         = PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst          = PARA.pst;            % from (0:1];	    // semiconducting CNTs to total CNTs ratio - [-]
scalingFactor = 1e6;

% DC parameters s-tubes
vdcr    = PARA.vdcr;
betdcr  = PARA.betdcr;
vthin   = PARA.vthin;
dvthin  = PARA.dvthin;
m1in    = PARA.m1in;
m2in    = PARA.m2in;
idstn   = PARA.idstn;
betds   = PARA.betds;
pin     = PARA.pin;

% Bias data
Vgd     = Vgs - Vds;

% model
u     = Vds/vdcr;
fds   = u./power(1+power(hypot(u,Vt),betdcr),(1/betdcr));
 
vthn2 = vthin + dvthin;
	
nom   = 1+exp((Vgs -vthin)/m1in/phit);
denom = 1+exp(          -vthin /m1in/phit);
arg1  = nom./denom;

nom   = 1+exp((Vgs-power(Vds,betds)-vthn2)/m2in/phit);
denom = 1+exp((   -power(Vds,betds)-vthn2)/m2in/phit);
arg2  = nom./denom;
	
smooth = (Vgs/vthin)./power((1+power(Vgs/vthin,2)),0.5);

fgsn = m1in*log(arg1)-pin*m2in*log(arg2).*smooth;

% result
curr = w*scalingFactor*pst*dens*idstn*fgsn.*fds;
 
end

% /////   Charge  model    ///////////////////////////////////////
function [Qs,Qd]=charge_emp(Vgs,Vds,PARA)

if (PARA.chargemod==1)
    [Qs,Qd] = CCAM_MATLAB_Qcnt_NANOHUB(Vgs,Vds,PARA);
else
    [Qs,Qd] = ccam_new_Qcnt(Vgs,Vds,PARA);
end  

end

% chargemod = 1
function [Qs_cnt,Qd_cnt] = CCAM_MATLAB_Qcnt_NANOHUB(Vgs,Vds,PARA)

% constant
Vt          = 0.0259;       % Thermal voltage [V]

% structural parmeters
w            = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens         = PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst          = PARA.pst;            % from (0:1];	    // semiconducting CNTs to total CNTs ratio - [-]
scalingFactor = 1e6;

%AC parameters s-tubes
ctn0    = PARA.ctn0;                          % from [0:1u];% charge factor
ctp0    = PARA.ctp0;                          % from [0:1u];% charge factor
vthqs   = PARA.vthqs;	                       % from [-10:10];%threshhold volatge for Qs
vth0qs  = PARA.vth0qs;                      % from (0:100];% fit parameter for Qs calculation
athqs   = PARA.athqs;                        % from (0:100];% fit parameter for Qs calculation
vthqd   = PARA.vthqd;	                       % from [-10:10];% threshhold voltage for Qd
vth0qd  = PARA.vth0qd;                       % from (0:100];% fit parameter for Qd calculation
athqd   = PARA.athqd;	                       % from (0:100];% fit parameter for Qd calculation
pqsd    = PARA.pqsd;	                       % from [0:1];% source/drain partitioning factor
    
% parameters for temperature dependence
dt          = PARA.dt;
alvthqs     = PARA.alvthqs;
alvth0qs    = PARA.alvth0qs;
alvthqd     = PARA.alvthqd;
alvth0qd    = PARA.alvth0qd;
alct0       = PARA.alct0;

% temperature dependent parameter modification
vthqs_t     = vthqs*(1 + alvthqs*dt);
vth0qs_t    = vth0qs*(1 + alvth0qs*dt);
vthqd_t     = vthqd * (1 + alvthqd*dt);
vth0qd_t    = vth0qd * (1 + alvth0qd*dt);
ctn0_t      = ctn0/(1 + alct0*dt);
ctp0_t      = ctp0/(1 + alct0*dt);

% Bias data
Vgd     = Vgs - Vds;

% model

% source
if (Vgs>vthqs_t)
    vg_t    = Vgs-vthqs_t;
    u       = zeros(length(vg_t),1);
    u(vg_t > 1e-6*Vt)   = 1 - vth0qs_t./vg_t(vg_t > 1e-6*Vt);
    u(vg_t <= 1e-6*Vt)  = 1 - vth0qs_t./(1e-6*Vt);

    w_s     = sqrt(u*u + athqs);
    w_u     = (u + w_s)/(1 + sqrt(1 + athqs));
    
    % result
    Qs_cnt  = w*scalingFactor*pst*dens*pqsd*ctn0_t*vg_t*w_u*w_u;
else
    vg_t    = -Vgs+vthqs_t;
    u       = zeros(length(vg_t),1);
    u(vg_t > 1e-6*Vt)   = 1 - vth0qs_t./vg_t(vg_t > 1e-6*Vt);
    u(vg_t <= 1e-6*Vt)  = 1 - vth0qs_t./(1e-6*Vt);
    
    w_s     = sqrt(u*u + athqs);
    w_u     = (u + w_s)/(1 + sqrt(1 + athqs));
    
    % result
    Qs_cnt  = -w*scalingFactor*pst*dens*pqsd*ctp0_t*vg_t*w_u*w_u;
end


% drain
if (Vgd>vthqd_t)
    vg_t    = Vgd - vthqs_t;
    u       = zeros(length(vg_t),1);
    u(vg_t > 1e-6*Vt)   = 1 - vth0qd_t./vg_t(vg_t > 1e-6*Vt);
    u(vg_t <= 1e-6*Vt)  = 1 - vth0qd_t./(1e-6*Vt);

    w_d     = sqrt(u*u + athqd);
    w_u     = (u + w_d)/(1 + sqrt(1+athqd));
    
    % result
    Qd_cnt  = w*scalingFactor*pst*dens*(1-pqsd)*ctn0_t*vg_t*w_u*w_u;
else
    vg_t    = -Vgd + vthqd_t;
    u       = zeros(length(vg_t),1);
    u(vg_t > 1e-6*Vt)   = 1 - vth0qd_t./vg_t(vg_t > 1e-6*Vt);
    u(vg_t <= 1e-6*Vt)  = 1 - vth0qd_t./(1e-6*Vt);
    
    w_d     = sqrt(u*u + athqd);
    w_u     = (u + w_d)/(1 + sqrt(1 + athqd));
    
    % result
    Qd_cnt= -w*scalingFactor*pst*dens*(1-pqsd)*ctp0_t*vg_t*w_u*w_u;
end

end

% chargemod = 2
function [Qs_cnt,Qd_cnt] = ccam_new_Qcnt(Vgs,Vds,PARA)

% constant
Vt          = 0.0259;       % Thermal voltage [V]

% structural parmeters
w            = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens         = PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst          = PARA.pst;            % from (0:1];	    // semiconducting CNTs to total CNTs ratio - [-]
scalingFactor = 1e6;

%D-Charge Parameters
ctn0    = PARA.ctn0; 
ctp0    = PARA.ctp0;
mqs     = PARA.mqs; 
vthqs   = PARA.vthqs;
mqd     = PARA.mqd; 
vthqd   = PARA.vthqd;
pqsd    = PARA.pqsd; 

% Bias data
Vgd     = Vgs - Vds;

% model

% source
vg_t   = Vgs - vthqs;
nom    = 1 + exp(  vg_t/mqs/phit);
denom  = 1 + exp(-vthqs/mqs/phit);
fqsn   = mqs*Vt*log(nom/denom );

% result
Qs_cnt = w*scalingFactor*pst*dens*pqsd*(ctn0*fqsn);


% drain
vg_t   = Vgd - vthqd;
nom    = 1 + exp(  vg_t/mqd/Vt);
denom  = 1 + exp(-vthqd/mqd/Vt);
fqdn   = mqd*Vt*log(nom/denom );

% result
Qd_cnt = w*scalingFactor*pst*dens*(1-pqsd)*(ctn0*fqdn);

end


% /////   Metalic tubes    ///////////////////////////////////////
function Imet_avmt0 = IM_avmt0_scat(Vds,PARA)

% constant
Vt          = 0.0259;       % Thermal voltage [V]

% structural parmeters
w            = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens         = PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst          = PARA.pst;            % from (0:1];	    // semiconducting CNTs to total CNTs ratio - [-]
scalingFactor = 1e6;
 
% parameters for metallic tubes
rmta    = PARA.rmta;
amto    = PARA.amto;
rscm    = PARA.rscm;
rdcm    = PARA.rdcm;    

% temperature dependent parameters
tnom    = 300;
Tvar_m  = tnom+PARA.dt;
rmta_t  = rmta*power(Tvar_m/T_0,PARA.zetrmta);
amto_t  = amto*power(Tvar_m/T_0,PARA.zetamto);

%model
rm = rmta_t + amto_t*sqrt(Vds.^2 + Vt^2);

% result
Imet_avmt0 = w*scalingFactor*(1-pst)*dens*abs(Vds)./(rdcm+rm+rscm);

end

function [Y11,Y12,Y21,Y22,Z11,Z12,Z21,Z22,S11,S12,S21,S22] = YPARAM(freq,V,PARA)

% Large Rg causes large real parts of Y parameters
% Resistors cause kinks in dQ to appear in all Y parameters (also real part)
% Why is yint different for different models???

scalingFactor = 1e6;

% structural parmaeters
w       = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens 	= PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst     = PARA.pst; 

Rsf     = PARA.rsf*w*scalingFactor;
Rdf     = PARA.rdf*w*scalingFactor;
Rg      = PARA.rg*w*scalingFactor;
Rsc     = PARA.rscs/(w*scalingFactor*pst*dens);
Rdc     = PARA.rdcs/(w*scalingFactor*pst*dens);
Rscm    = PARA.rscm/(w*scalingFactor*pst*dens);
Rdcm    = PARA.rdcm/(w*scalingFactor*pst*dens);

Cmtp    = PARA.cmt;
pcmt    = PARA.pqmt;


% Only cgspar2, cgdpar2 used!!!
cgsims1 = PARA.cgspar1;
cgdims1 = PARA.cgdpar1;
cgsims2 = PARA.cgspar2*w*scalingFactor;
cgdims2 = PARA.cgdpar2*w*scalingFactor;
cdsims  = PARA.cdspar*w*scalingFactor;

s=1i.*2.*pi.*freq;

for ct_s=1:length(s)
    % small signal currents of semiconducting tubes
    deltaV=0.001;
    biasdata_vgs1=V.Vgisi-deltaV/2;
    biasdata_vgs2=V.Vgisi+deltaV/2;
    biasdata_vds1=V.Vdisi-deltaV/2;
    biasdata_vds2=V.Vdisi+deltaV/2;

    [IS_vgs1]=IS_emp(biasdata_vgs1,V.Vdisi,PARA);
    [IS_vgs2]=IS_emp(biasdata_vgs2,V.Vdisi,PARA);
    [IS_vds1]=IS_emp(V.Vgisi,biasdata_vds1,PARA);
    [IS_vds2]=IS_emp(V.Vgisi,biasdata_vds2,PARA);
    [Q_s_vgs1,Q_d_vgs1]=charge_emp(biasdata_vgs1,V.Vdisi,PARA);
    [Q_s_vgs2,Q_d_vgs2]=charge_emp(biasdata_vgs2,V.Vdisi,PARA);
    [Q_s_vds1,Q_d_vds1]=charge_emp(V.Vgisi,biasdata_vds1,PARA);
    [Q_s_vds2,Q_d_vds2]=charge_emp(V.Vgisi,biasdata_vds2,PARA);

    gm=(IS_vgs2-IS_vgs1)/deltaV;
    gds=(IS_vds2-IS_vds1)/deltaV;

    %dQs/dVgs
    dqsdvgs=abs((Q_s_vgs2-Q_s_vgs1)/deltaV);
    %dQs/dVds
%     dqsdvds=abs((Q_s_vds2-Q_s_vds1)/deltaV);
    dqsdvds=0;
    %dQd/dVgs
    dqddvgs=abs((Q_d_vgs2-Q_d_vgs1)/deltaV);
    %dQd/dVds
    dqddvds=abs((Q_d_vds2-Q_d_vds1)/deltaV);


    yint(1,1)=0+s(ct_s)*(dqsdvgs+dqddvgs);
    yint(1,2)=0-s(ct_s)*(dqddvds+dqsdvds);
    yint(2,1)=gm-s(ct_s)*dqddvgs;
    yint(2,2)=gds+s(ct_s)*dqddvds;


    zint=y2z(yint);

    % add contact resistances
    ze1(1,1)=zint(1,1)+Rsc;
    ze1(1,2)=zint(1,2)+Rsc;
    ze1(2,1)=zint(2,1)+Rsc;
    ze1(2,2)=zint(2,2)+Rsc+Rdc;

    ye1=z2y(ze1);

    % calculate model part for metallic tubes
    if PARA.rmta==0
        gmt     = 0;
    else
        PARA.rscm = 0;
        PARA.rdcm = 0;
        
        deltaV=0.001;

        biasdata_vds1   = V.Vdmsm-deltaV/2;
        biasdata_vds2   = V.Vdmsm+deltaV/2;

        [Imt_vds1] = IM_avmt0_scat(biasdata_vds1,PARA);
        [Imt_vds2] = IM_avmt0_scat(biasdata_vds2,PARA);

        gmt = (Imt_vds2-Imt_vds1)/deltaV;
        
        PARA.rscm = Rscm;
        PARA.rdcm = Rdcm;
    end
    
    Cmtgs = (1-pcmt)*w*scalingFactor*(1-pst)*dens*Cmtp;
    Cmtgd = pcmt*w*scalingFactor*(1-pst)*dens*Cmtp;

    if Cmtp > 0
        % ymt(1,1)=2*s*(Cmtgs+Cmtgd);
        ymt(1,1)    = s(ct_s)*(Cmtgs+Cmtgd);
        ymt(1,2)    = -s(ct_s)*Cmtgd;
        ymt(2,1)    = -s(ct_s)*Cmtgd;
        ymt(2,2)    = gmt+s(ct_s)*Cmtgd;
        zmt         = y2z(ymt);

        zmt(1,1)    = zmt(1,1)+Rscm;
        zmt(1,2)    = zmt(1,2)+Rscm;
        zmt(2,1)    = zmt(2,1)+Rscm;
        zmt(2,2)    = zmt(2,2)+Rscm+Rdcm;
        ymt         = y2z(zmt);
    else
        ymt         = zeros(2,2);
        ymt(2,2)    = 1/(Rscm+Rdcm+1./gmt);
    %     ymt(2,2)=(Rscm*Rdcm/(Rscm+Rdcm))*(1./gmt)./((Rscm*Rdcm/(Rscm+Rdcm))+1./gmt);
    end

    % parallel connection of semiconductive and mt-model up to nodes gi,sx,dx
    if any(any(isnan(ye1)))||any(any(isinf(ye1)))
        ye1=zeros(2,2);
        disp('NaN or inf in ye1');
    end

    ye2 = ye1+ymt+[s(ct_s)*(cgsims2+cgdims2),-s(ct_s)*cgdims2;-s(ct_s)*cgdims2,s(ct_s)*cgdims2];

    z   = y2z(ye2) + [Rsf+Rg, Rsf; Rsf, Rsf+Rdf];

    % external Y-parameters
    y=z2y(z);
    Y11(ct_s)=y(1,1)+s(ct_s)*(cgsims1+cgdims1);
    Y12(ct_s)=y(1,2)-s(ct_s)*cgdims1;
    Y21(ct_s)=y(2,1)-s(ct_s)*cgdims1;
    Y22(ct_s)=y(2,2)+s(ct_s)*(cdsims+cgdims1);
    
    % external Z-parameters
    zel=y2z(y);
    Z11(ct_s)=zel(1,1);
    Z12(ct_s)=zel(1,2);
    Z21(ct_s)=zel(2,1);
    Z22(ct_s)=zel(2,2);
    
    % external S-parameters
    s=y2s(y);
    S11(ct_s)=s(1,1);
    S12(ct_s)=s(1,2);
    S21(ct_s)=s(2,1);
    S22(ct_s)=s(2,2);
end

end






