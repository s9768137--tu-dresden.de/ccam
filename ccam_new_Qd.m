% CNTFET compact model

function Qd_cnt=ccam_new_Qd(Vd,Vg,Vs,PARA)

phit = 8.617e-5*300;   % Thermal voltage [V]

scalingFactor = 1e6;

% type modifier
devtype    = PARA.devtype;

% structural parmaeters
w       = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens 	= PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst     = PARA.pst; 

%D-Charge Parameters

ctn0  = PARA.ctn0; 
ctp0  = PARA.ctp0;
mqd   = PARA.mqd; 
vthqd = PARA.vthqd;
pqsd  = PARA.pqsd; 

Vgd = Vg - Vd;

Vgd=devtype*Vgd;


% /////   Charge  model    ///////////////////////////////////////

% //Qd
 
vg_t   = Vgd-vthqd;
		
nom    = 1+exp(  vg_t /mqd/phit);
denom  = 1+exp(-vthqd /mqd/phit);
fqdn   = mqd*phit*log( nom / denom );
Qd_cnt = devtype*(1-pqsd)*w*scalingFactor*pst*dens*(ctn0*fqdn);

end



