function [ID,Vdmsm,Vdisi,Vgisi,INTERN]=solve_internal_model(Vgs,Vds,PARA)

INTERN=[];

options=optimset('DiffMinChange',1e-3,'DiffMaxChange',1e1,'display','off','tolfun',1e-6);
Vext=[Vgs,Vds];
if ~isfield(PARA,'currmod')
    PARA.currmod=1;
end
%% NEWTON for contact resistance
V=fsolve(@(Vint)F_IT(Vint,Vext,PARA,1),Vext,options);
% V=Vext;
OUT=F_IT(V,Vext,PARA,0);
ID=OUT.ID;
Vdmsm=OUT.Vdmsm;
Vdisi=OUT.Vdisi; 
Vgisi=OUT.Vgisi;
[INTERN.Qs,INTERN.Qd]=charge_emp(Vgisi',Vdisi',PARA);
