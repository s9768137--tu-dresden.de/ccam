function [Qs,Qd]=charge_emp(Vg,Vd,PARA)

%vt=PARA.vt;
if (PARA.chargemod==1)
    Qd = CCAM_MATLAB_QD_NANOHUB(Vd,Vg,0,PARA);
    Qs = CCAM_MATLAB_QS_NANOHUB(Vd,Vg,0,PARA);
else 
    Qd = ccam_new_Qd(Vd,Vg,0,PARA);
    Qs = ccam_new_Qs(Vd,Vg,0,PARA);    
end    
end

