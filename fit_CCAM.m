function funVal = fit_CCAM(params01,xdata,LBounds, UBounds,para,ref,fit,fitFlag,freq)

params = params01.*(UBounds-LBounds)+LBounds;
% update variable parameters

for n=1:length(fit.para)
  para.(fit.para{n})  = params(n);
end

para.rdcs = para.rscs;
para.rdcm = para.rscm;

%parasitics
para.cgdpar2 = para.cgspar2;

ID = zeros(size(ref,1),1);
Qs = zeros(size(ref,1),1);
Qd = zeros(size(ref,1),1);

Vg = unique(ref(:,1));
Vd = unique(ref(:,2));

funVal = [];

set_ADS_conf;

if fitFlag == 1
    ID_large=[]; 
    for i=1:length(Vd)
        VdVec = repmat(Vd(i),length(Vg),1);
        ID = CCAM_v220_matlab(para,unique(ref(:,1)),0,Vd(i));
        ID_large = [ID_large; ID];
    end
    funVal = ID_large*1e6;
    
elseif fitFlag == 2    
    gm_large=[]; 
    for i=1:length(Vd)
        VdVec = repmat(Vd(i),length(Vg),1);
        ID = CCAM_v220_matlab(para,unique(ref(:,1)),0,Vd(i));
        gm = gradient(ID)./gradient(Vg);
        gm_large = [gm_large; gm];
    end
    funVal = gm_large*1e6;

elseif fitFlag == 3
    clear YP SP ZP;
    fT_large=[]; fmax_large=[];
    for i=1:length(Vd)
        VdVec = repmat(Vd(i),length(Vg),1);
        [~,~,~,YP(i),SP(i),ZP] = CCAM_v220_matlab(para,unique(ref(:,1)),0,Vd(i),freq);
        [ft(:,i),fmax(:,i)] = calc_freq_fom(YP{1},SP{1},ZP{1},1);
        fT_large = [fT_large;ft(:,i)];
        fmax_large = [fmax_large;fmax(:,i)];
    end
    funVal = [fT_large;fmax_large]*1e-9;
    
elseif fitFlag == 4
    clear YP SP ZP;
    fT_large=[];
    for i=1:length(Vd)
        VdVec = repmat(Vd(i),length(Vg),1);
        [~,~,~,YP(i),SP(i),ZP] = CCAM_v220_matlab(para,unique(ref(:,1)),0,Vd(i),freq);
        [ft(:,i)] = calc_freq_fom(YP{1},SP{1},ZP{1},1);
        fT_large = [fT_large;ft(:,i)];
    end
    funVal = [fT_large]*1e-9;
    
elseif fitFlag == 5
    clear YP SP ZP;
    fmax_large=[];
    for i=1:length(Vd)
        VdVec = repmat(Vd(i),length(Vg),1);
        [~,~,~,YP(i),SP(i),ZP] = CCAM_v220_matlab(para,unique(ref(:,1)),0,Vd(i),freq);
        [~,fmax(:,i)] = calc_freq_fom(YP{1},SP{1},ZP{1},1);
        fmax_large = [fmax_large;fmax(:,i)];
    end
    funVal = [fmax_large]*1e-9;
    
elseif fitFlag == 6
    clear YP SP ZP;
    Cgs_large=[]; Cgd_large=[]; Cds_large=[];
    for i=1:length(Vd)
        VdVec = repmat(Vd(i),length(Vg),1);
        [~,~,~,YP,SP,ZP] = CCAM_v220_matlab(para,unique(ref(:,1)),0,Vd(i),freq);
        [~,~,Cgs(:,i),Cgd(:,i),Cds(:,i)] = calc_freq_fom(YP{1},SP{1},ZP{1},1);
        Cgs_large = [Cgs_large;Cgs(:,i)];
        Cgd_large = [Cgd_large;Cgd(:,i)];
        Cds_large = [Cds_large;Cds(:,i)];
    end
    funVal = [Cgs_large; Cgd_large; Cds_large]*1e15;
    
elseif fitFlag == 7
    clear YP SP ZP;
    fT_large=[]; fmax_large=[]; Cgs_large=[]; Cgd_large=[]; Cds_large=[];
    for i=1:length(Vd)
        VdVec = repmat(Vd(i),length(Vg),1);
        [~,~,~,YP(i),SP(i),ZP] = CCAM_v220_matlab(para,unique(ref(:,1)),0,Vd(i),freq);
        [ft(:,i),fmax(:,i),Cgs(:,i),Cgd(:,i),Cds(:,i)] = calc_freq_fom(YP{1},SP{1},ZP{1},1);
        fT_large = [fT_large;ft(:,i)];
        fmax_large = [fmax_large;fmax(:,i)];
        Cgs_large = [Cgs_large;Cgs(:,i)];
        Cgd_large = [Cgd_large;Cgd(:,i)];
        Cds_large = [Cds_large;Cds(:,i)];
    end
    funVal = [fT_large*1e-9; fmax_large*1e-9; Cgs_large*1e15; Cgd_large*1e15; Cds_large*1e15];
elseif fitFlag == 8
    clear YP SP ZP;
    Y11_large=[]; Y12_large=[]; Y21_large=[]; Y22_large=[];
    for i=1:length(Vd)
        VdVec = repmat(Vd(i),length(Vg),1);
        [~,~,~,YP(i),SP(i),ZP] = CCAM_v220_matlab(para,unique(ref(:,1)),0,Vd(i),freq);
        Y11_large = [real(YP{1}.Y11');imag(YP{1}.Y11')];
        Y12_large = [real(YP{1}.Y12');imag(YP{1}.Y12')];
        Y21_large = [real(YP{1}.Y21');imag(YP{1}.Y21')];
        Y22_large = [real(YP{1}.Y22');imag(YP{1}.Y22')];
    end
    funVal = [Y11_large; Y12_large; Y21_large; Y22_large]*1e6;
end

end