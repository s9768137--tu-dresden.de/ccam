function [Id,Qs,Qd,YP,SP,ZP]=CCAM_v210(VGvec,VDvec,Fvec,PARA,Vs)

Fvec=unique(Fvec);

if nargin == 5
    Vsi = Vs;
else
    Vsi = 0;
end

if max(Fvec)>0
  isY=true;
else
  isY=false;
end

if ~isfield(PARA,'dt'); PARA.dt=0; end;

delta_T_s = 0;
delta_T_m = 0;


% if one contact resistance defined overwrite contact resistances
if isfield(PARA,'rcs')
    PARA.rscs=PARA.rcs;
    PARA.rdcs=PARA.rcs;
end
if isfield(PARA,'rcm')
    PARA.rscm=PARA.rcm;
    PARA.rdcm=PARA.rcm;
end

PARA.rscs=PARA.rscs*(1+PARA.alrscon*delta_T_s);
PARA.rdcs=PARA.rdcs*(1+PARA.alrscon*delta_T_m);

PARA.rscm=PARA.rscm*(1+PARA.alrmcon*PARA.dt);
PARA.rdcm=PARA.rdcm*(1+PARA.alrmcon*PARA.dt);

% set p-type or n-type
if ~isfield(PARA,'devtype'); PARA.devtype=1; end;

%  calculate OP
for k = 1:length(unique(VGvec))
    for l = 1:length(unique(VDvec))
        [Id(k,l),Vdmsm(k,l),Vdisi(k,l),Vgisi(k,l),INTERN]=solve_internal_model(VGvec(k),VDvec(l),PARA);

        Qs(k,l)=INTERN.Qs;
        Qd(k,l)=INTERN.Qd;
    end
end

if max(Fvec)<=0
    Y11=0;
    Y12=0;
    Y21=0;
    Y22=0;
else
    %% calc Y-Parameters
    for k = 1:length(VGvec)
        for l = 1:length(unique(VDvec))
            for freqIndex = 1:length(Fvec)
                
                V.Vgisi = Vgisi(k,l);
                V.Vdisi = Vdisi(k,l);
                V.Vdmsm = Vdmsm(k,l);
                [Y11,Y12,Y21,Y22,Z11,Z12,Z21,Z22,S11,S12,S21,S22] = YPARAM(Fvec(freqIndex),V,PARA);
                
                YP{l}.freq(1,freqIndex) = Fvec(freqIndex);
                YP{l}.Y11(k,freqIndex) = Y11;
                YP{l}.Y12(k,freqIndex) = Y12;
                YP{l}.Y21(k,freqIndex) = Y21;
                YP{l}.Y22(k,freqIndex) = Y22;
                
                ZP{l}.freq(1,freqIndex) = Fvec(freqIndex);
                ZP{l}.Z11(k,freqIndex) = Z11;
                ZP{l}.Z12(k,freqIndex) = Z12;
                ZP{l}.Z21(k,freqIndex) = Z21;
                ZP{l}.Z22(k,freqIndex) = Z22;
                
                SP{l}.freq(1,freqIndex) = Fvec(freqIndex);
                SP{l}.S11(k,freqIndex) = S11;
                SP{l}.S12(k,freqIndex) = S12;
                SP{l}.S21(k,freqIndex) = S21;
                SP{l}.S22(k,freqIndex) = S22;
            end
        end
    end
end

end
