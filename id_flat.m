% CNTFET compact model

function curr=id_flat(Vd,Vg,Vs,PARA)


phit = 8.617e-5*300;   % Thermal voltage [V]
scalingFactor = 1e6;

% type modifier parameter
devtype = PARA.devtype;

% structural parmaeters
w       = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens 	= PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst     = PARA.pst;            % from (0:1];	    // semiconducting CNTs to total CNTs ratio - [-]

%DC parameters s-tubes
vdcr    = PARA.vdcr;
betdcr  = PARA.betdcr;
vthin   = PARA.vthin;
dvthin  = PARA.dvthin;
m1in    = PARA.m1in;
m2in    = PARA.m2in;
m3in    = PARA.m3in;
idstn   = PARA.idstn;
betds   = PARA.betds;
pin     = PARA.pin;

vthip   = PARA.vthip;
dvthip  = PARA.dvthip;
m1ip    = PARA.m1ip;
m2ip    = PARA.m2ip;
m3ip    = PARA.m3ip;
idstp   = PARA.idstp;
pip     = PARA.pip;


%%
vgs = Vg - Vs;
vds = Vd - Vs;

% type modifier for p-type and n-type
vds=devtype*vds;
vgs=devtype*vgs;

% current  fds
u     = vds/vdcr;
fds   = u./power(1+power(hypot(u,phit),betdcr),(1/betdcr));
 

% electron current   fgsn
vthn2 = vthin + dvthin;

nom   = 1+exp((vgs-vthin)/m1in/phit);
denom = 1+exp(    -vthin /m1in/phit);
arg1 = nom.*(1/(1+exp((vgs-m3in*phit)/phit))) + (nom./denom).*(1-1/(1+exp((vgs-m3in*phit)/phit)));

nom   = 1+exp((vgs-power(abs(vds),betds)-vthn2)/m2in/phit);
denom = 1+exp((   -power(abs(vds),betds)-vthn2)/m2in/phit);
arg2  = nom./denom;

smooth1 = (vgs/vthin)./power((1+power(vgs/vthin,2)),0.5);
fgsn = m1in*log(arg1)-pin*m2in*log(arg2).*smooth1;


% hole current   fgsp
vthp2 = vthip + dvthip;
nom   = 1+exp((-vgs+vthip)/m1ip/phit);
denom = 1+exp(      vthip /m1ip/phit);
arg1 = nom.*(1/(1+exp((-vgs-m3ip*phit)/phit))) + (nom./denom).*(1-1/(1+exp((-vgs-m3ip*phit)/phit)));

nom   = 1+exp((-vgs+power(abs(vds),betds)+vthp2)/m2ip/phit);
denom = 1+exp((     power(abs(vds),betds)+vthp2)/m2ip/phit);
arg2  = nom./denom;

smooth1 = (vgs/vthip)./power((1+power(vgs/vthip,2)),0.5);
fgsp = m1ip*log(arg1)-pip*m2ip*log(arg2).*smooth1;


% --> final current 
curr = devtype*w*scalingFactor*pst*dens*(idstn*fgsn + idstp.*fgsp).*fds;
 
end

