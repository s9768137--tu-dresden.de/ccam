function IS=IS_emp(vgs,vds,PARA)
%vgsdata=biasdata(:,1);
%vdsdata=biasdata(:,2);
%ids0=PARA.ids0;

if (PARA.currmod==1)
    IS=CCAM_MATLAB_IDxVG_NANOHUB(vds,vgs,0,PARA);
else
    IS = id_flat(vds,vgs,0,PARA);
end    

end
