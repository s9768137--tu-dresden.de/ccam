function [F]=F_IT(Vint,Vext,PARA,flag)

scalingFactor = 1e6;

%% structural parmaeters
w       = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens 	= PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst     = PARA.pst; 

%% input Parameter
Rsc=PARA.rscs;
Rdc=PARA.rdcs;
Rsf=PARA.rsf;
Rdf=PARA.rdf;

%% separate Vgs, Vds
Vgisi=Vint(1:length(Vint)/2);
Vdisi=Vint(length(Vint)/2+1:end);

if flag
    PARA.plot_internal_values=0;
else
    PARA.plot_internal_values=1;
end

%% different models
ISEM=IS_emp(Vgisi',Vdisi',PARA)';

%% voltage drop contact resistance SEM
Vdxsx=Vdisi+ISEM*(Rsc+Rdc)/(w*scalingFactor*pst*dens);
Vgsx=Vgisi+ISEM*(Rsc)/(w*scalingFactor*pst*dens);

%% metallic tubes
if PARA.rmta>0
%     if PARA.amto>0
%         IMET=IM_scat([zeros(length(Vdxsx),1),Vdxsx'],PARA)';
%     else
        IMET=IM_avmt0_scat([zeros(length(Vdxsx),1),Vdxsx'],PARA)';
%     end
else
    IMET=0;
end
        

%% voltage drop finger resistance
Vgs=Vgsx+(ISEM+IMET)*Rsf*w*scalingFactor;
Vds=Vdxsx+(ISEM+IMET)*(Rsf+Rdf)*w*scalingFactor;

if any(imag(Vgs))
    disp('Strom ist komplex') 
end
%% save
if flag %% for newton
    Vsolve=[Vgs,Vds];
    F=Vext-Vsolve;
else
    Rsm=PARA.rscm;
    Rdm=PARA.rdcm;
    F.ID=ISEM+IMET;
    F.Vdmsm=Vdxsx-IMET*(Rsm+Rdm)/(w*scalingFactor*(1-pst)*dens);
    F.Vdisi=Vdisi;
    F.Vgisi=Vgisi;
end
