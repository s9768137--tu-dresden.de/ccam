function CCAMv212_ModelCard_Updater(inputFile,w,dens,pst,simulator)
% Input arguments: 
%   inputFile   - Input netlist file (*.mat and *.net allowed)
%   w           - width of the CNTFET (in m)
%   dens        - tube density of the CNTFET (in per um)
%   pst         - semiconducting CNTs to total CNTs ratio
%   simulator   - 'ADS' or 'spectre'
% Output arguments:
%   Nothing

if (exist(inputFile,'file') ~= 2)
    exit('Input netlist file not found!')
else
    if ~isempty(regexp(inputFile,'\w*_ccam_v(\d*)_\w*'))
        match       = regexp(inputFile,'(\w*_ccam_v)\d*_(\w*).(\w*)','tokens');
        fileName    = [match{1}{1},'212_',match{1}{2},'.',match{1}{3}];
    else
        match       = regexp(inputFile,'(\w*).(\w*)','tokens');
        fileName    = [match{1}{1},'_ccam_v212.',match{1}{2}];
    end
end

isMatFile = 0; isNetFile = 0;
if ~isempty(regexp(inputFile,'\w*.mat','once'))
    matFileData = load(inputFile);
    PARA = matFileData.PARA;
    isMatFile = 1;
    
elseif ~isempty(regexp(inputFile,'\w*.net','once'))
    if (nargin < 4)
        exit('Simulator argument needed for identifying the format of netlist file!')
    end
    PARA = readNetFile(inputFile,simulator);
    isNetFile = 1;
end

PARA_v212 = PARA;
scalingFactor = 1e6;

% Width and tube density scaling parameter
PARA_v212.w     = w;
PARA_v212.dens  = dens;
PARA_v212.pst   = pst;

% DC parameters of semiconducting tubes
if isfield(PARA,'idstn');   PARA_v212.idstn = PARA.idstn/(w*scalingFactor*dens*pst);      end
if isfield(PARA,'idstp');   PARA_v212.idstp = PARA.idstp/(w*scalingFactor*dens*pst);      end
if isfield(PARA,'rscs');    PARA_v212.rscs = PARA.rscs*(w*scalingFactor*dens*pst);        end
if isfield(PARA,'rdcs');    PARA_v212.rdcs = PARA.rdcs*(w*scalingFactor*dens*pst);        end

% AC parameters of semiconducting tubes
if isfield(PARA,'ctn0');    PARA_v212.ctn0 = PARA.ctn0/(w*scalingFactor*dens*pst);        end
if isfield(PARA,'ctp0');    PARA_v212.ctp0 = PARA.ctp0/(w*scalingFactor*dens*pst);        end

% Parameters of metallic tubes
if isfield(PARA,'rmta');    PARA_v212.rmta = PARA.rmta*(w*scalingFactor*dens*(1-pst));    end
if isfield(PARA,'amto');    PARA_v212.amto = PARA.amto*(w*scalingFactor*dens*(1-pst));    end
if isfield(PARA,'cmt')
    if pst<1
        PARA_v212.cmt = PARA.cmt/(w*scalingFactor*dens*(1-pst));
    else
        PARA_v212.cmt = 0;
    end
end
if isfield(PARA,'rscm');    PARA_v212.rscm = PARA.rscm*(w*scalingFactor*dens*(1-pst));    end
if isfield(PARA,'rdcm');    PARA_v212.rdcm = PARA.rdcm*(w*scalingFactor*dens*(1-pst));    end

% External parameters
if isfield(PARA,'rsf');         PARA_v212.rsf = PARA.rsf/(w*scalingFactor);         end
if isfield(PARA,'rdf');         PARA_v212.rdf = PARA.rdf/(w*scalingFactor);         end
if isfield(PARA,'rg');          PARA_v212.rg = PARA.rg/(w*scalingFactor);           end
if isfield(PARA,'cgspar2');     PARA_v212.cgspar2 = PARA.cgspar2/(w*scalingFactor); end
if isfield(PARA,'cgdpar2');     PARA_v212.cgdpar2 = PARA.cgdpar2/(w*scalingFactor); end
if isfield(PARA,'cdspar');      PARA_v212.cdspar = PARA.cdspar/(w*scalingFactor);   end

if isMatFile
    PARA = PARA_v212;
    save(fileName,'PARA');
end

if isNetFile
    para2net(PARA_v212,fileName,simulator,'CCAM');   
end

end

function PARA = readNetFile(netFile,simulator)
% Input arguments: 
%   netFile   - net file name
%   simulator - 'ADS', 'spectre'
% Output arguments:
%   PARA      - Matlab Parameter structure

if (exist(netFile,'file') ~= 2)
    exit('netlist file not found!')
end

if (nargin < 2)
    exit('Simulator needed for identifying the netlist file!')
end

match= [];

netfid = fopen(netFile,'r'); 
if (strcmp(simulator,'ADS'))
    while ~feof(netfid)
        line = fgets(netfid); %# read line by line
        if strncmp(line,'model',5)
            match = [match,regexp(line,' (\w*)=(-*\d*\.*\d*e*-*\w*)\s\\*','tokens')];
        else
            match = [match,regexp(line,'(\w*)=(-*\d*\.*\d*e*-*\w*)\s\\*','tokens')];
        end
    end
elseif (strcmp(simulator,'spectre'))
    while ~feof(netfid)
        line = fgets(netfid); %# read line by line
        match = [match,regexp(line,'+\s*(\w*)=(-*\d*\.*\d*e*-*\w*)','tokens')];
    end
end
fclose(netfid);

if isempty(match)
    exit('No parameters matched in netlist file!');
end

PARA = [];
for i = 1:size(match,2)
    PARA = setfield(PARA,{1},match{i}{1},{1},str2double(match{i}{2}));
end

if isempty(PARA)
    exit('No parameters found in netlist file!');
end

end
