function [fit_param] = fit_linearFun(params,para,xdata,fit)

for n=1:length(fit.para)
  para.(fit.para{n})  = params(n);
end

fit_param = linearFunction(para,xdata);
% fit_param = xdata.*para.m + para.c;

end