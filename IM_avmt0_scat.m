function Imet_avmt0 = IM_avmt0_scat(biasdata,PARA)

Vt = 0.0259;

%parameters m-tubes
rmta = PARA.rmta; %30;                          % from [0:1G];% low voltage resistance of metallic CNTs
amto = PARA.amto; %25;                          % from [0:1M];% saturation parameter of metallic CNTs
rscm = PARA.rscm; %8;                           % from [0:1M];% source contact resistance of m-tubes
rdcm = PARA.rdcm; %8;                           % from [0:1M];% drain contact resistance of m-tubes
tnom = PARA.tnom; % 27;

%////////////////////////////////////////////////////////////////
%/////   Metallic tube temperature dependant prefactors  //////////////////////
%////////////////////////////////////////////////////////////////
T_0 = tnom + 273;
Tvar_m = T_0 + PARA.dt;
% Rscm_t and Rdscm_t are already calculated in main file
rmta_t = rmta * power(Tvar_m/T_0,PARA.zetrmta);
amto_t = amto * power(Tvar_m/T_0,PARA.zetamto);

vgsdata=biasdata(:,1);
vdsdata=biasdata(:,2);

rm = rmta_t + amto_t*sqrt(vdsdata.^2 + Vt^2);
Imet_avmt0 = w*scalingFactor*(1-pst)*dens*abs(vdsdata)./(rdcm+rm+rscm);

end
