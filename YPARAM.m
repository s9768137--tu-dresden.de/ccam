function [Y11,Y12,Y21,Y22,Z11,Z12,Z21,Z22,S11,S12,S21,S22]=YPARAM(freq,V,PARA)

% Large Rg causes large real parts of Y parameters
% Resistors cause kinks in dQ to appear in all Y parameters (also real part)
% Why is yint different for different models???



%% if just DC
if all(freq==0)
  Y11=0;
  Y12=0;
  Y21=0;
  Y22=0;
  Z11=0;
  Z12=0;
  Z21=0;
  Z22=0;
  S11=0;
  S12=0;
  S21=0;
  S22=0;
  return
end

scalingFactor = 1e6;

% structural parmaeters
w       = PARA.w;              % from [0:inf);   // total transistor width - [m]
dens 	= PARA.dens;           % from [1:inf);	// density of CNT per um - [1/um]
pst     = PARA.pst; 

Rsf=PARA.rsf*w*scalingFactor;
Rdf=PARA.rdf*w*scalingFactor;
Rg=PARA.rg*w*scalingFactor;
Rsc=PARA.rscs/(w*scalingFactor*pst*dens);
Rdc=PARA.rdcs/(w*scalingFactor*pst*dens);
Rscm=PARA.rscm/(w*scalingFactor*pst*dens);
Rdcm=PARA.rdcm/(w*scalingFactor*pst*dens);

Cmtp=PARA.cmt;
pcmt=PARA.pqmt;

%%%%%%%%%%%%%%%%%%%%%
%% Only cgspar2, cgdpar2 used!!!
%%%%%%%%%%%%%%%%%%%%%
cgsims1=PARA.cgspar1;
cgdims1=PARA.cgdpar1;
cgsims2=PARA.cgspar2*w*scalingFactor;
cgdims2=PARA.cgdpar2*w*scalingFactor;
cdsims=PARA.cdspar*w*scalingFactor;

s=1i.*2.*pi.*freq;

for ct_s=1:length(s)
    %% small signal currents of semiconducting tubes

        deltaV=0.001;
        biasdata_vgs1=V.Vgisi-deltaV/2;
        biasdata_vgs2=V.Vgisi+deltaV/2;
        biasdata_vds1=V.Vdisi-deltaV/2;
        biasdata_vds2=V.Vdisi+deltaV/2;

        [IS_vgs1]=IS_emp(biasdata_vgs1,V.Vdisi,PARA);
        [IS_vgs2]=IS_emp(biasdata_vgs2,V.Vdisi,PARA);
        [IS_vds1]=IS_emp(V.Vgisi,biasdata_vds1,PARA);
        [IS_vds2]=IS_emp(V.Vgisi,biasdata_vds2,PARA);
        [Q_s_vgs1,Q_d_vgs1]=charge_emp(biasdata_vgs1,V.Vdisi,PARA);
        [Q_s_vgs2,Q_d_vgs2]=charge_emp(biasdata_vgs2,V.Vdisi,PARA);
        [Q_s_vds1,Q_d_vds1]=charge_emp(V.Vgisi,biasdata_vds1,PARA);
        [Q_s_vds2,Q_d_vds2]=charge_emp(V.Vgisi,biasdata_vds2,PARA);

        gm=(IS_vgs2-IS_vgs1)/deltaV;
        gds=(IS_vds2-IS_vds1)/deltaV;

        %dQs/dVgs
        dqsdvgs=abs((Q_s_vgs2-Q_s_vgs1)/deltaV);
        %dQs/dVds
    %     dqsdvds=abs((Q_s_vds2-Q_s_vds1)/deltaV);
        dqsdvds=0;
        %dQd/dVgs
        dqddvgs=abs((Q_d_vgs2-Q_d_vgs1)/deltaV);
        %dQd/dVds
        dqddvds=abs((Q_d_vds2-Q_d_vds1)/deltaV);


        yint(1,1)=0+s(ct_s)*(dqsdvgs+dqddvgs);
        yint(1,2)=0-s(ct_s)*(dqddvds+dqsdvds);
        yint(2,1)=gm-s(ct_s)*dqddvgs;
        yint(2,2)=gds+s(ct_s)*dqddvds;


    zint=y2z(yint);

    %% add contact resistances %%
    ze1(1,1)=zint(1,1)+Rsc;
    ze1(1,2)=zint(1,2)+Rsc;
    ze1(2,1)=zint(2,1)+Rsc;
    ze1(2,2)=zint(2,2)+Rsc+Rdc;

    ye1=z2y(ze1);

    %% %%%% calculate model part for metallic tubes %%%%

    if PARA.rmta==0
        gmt=0;
%     elseif PARA.amto==0
%         PARA.rscm=0;
%         PARA.rdcm=0;
%         gmt=dIM_avmt0dVd_scat([zeros(size(V.Vgisi)),V.Vdmsm],PARA);
    else
        PARA.rscm=0;
        PARA.rdcm=0;
        gmt=dIMdVd_scat([zeros(size(V.Vgisi)),V.Vdmsm],PARA);
    end

    Cmtgs=(1-pcmt)*w*scalingFactor*(1-pst)*dens*Cmtp;
    Cmtgd=pcmt*w*scalingFactor*(1-pst)*dens*Cmtp;

    if Cmtp>0
        % ymt(1,1)=2*s*(Cmtgs+Cmtgd);
        ymt(1,1)=s(ct_s)*(Cmtgs+Cmtgd);
        ymt(1,2)=-s(ct_s)*Cmtgd;
        ymt(2,1)=-s(ct_s)*Cmtgd;
        ymt(2,2)=gmt+s(ct_s)*Cmtgd;

        zmt=y2z(ymt);

        zmt(1,1)=zmt(1,1)+Rscm;
        zmt(1,2)=zmt(1,2)+Rscm;
        zmt(2,1)=zmt(2,1)+Rscm;
        zmt(2,2)=zmt(2,2)+Rscm+Rdcm;

        ymt=y2z(zmt);
    else
        ymt=zeros(2,2);
        ymt(2,2)=1/(Rscm+Rdcm+1./gmt);
    %     ymt(2,2)=(Rscm*Rdcm/(Rscm+Rdcm))*(1./gmt)./((Rscm*Rdcm/(Rscm+Rdcm))+1./gmt);
    end

    %% %% parallel connection of semiconductive and mt-model up to nodes gi,sx,dx %%%%
    if any(any(isnan(ye1)))||any(any(isinf(ye1)))
        ye1=zeros(2,2);
        disp('NaN or inf in ye1')
    end

    ye2=ye1+ymt+[s(ct_s)*(cgsims2+cgdims2),-s(ct_s)*cgdims2;-s(ct_s)*cgdims2,s(ct_s)*cgdims2];

    z=y2z(ye2)+[Rsf+Rg,Rsf;Rsf,Rsf+Rdf];

    %%%% external Y-parameters %%%%
    y=z2y(z);
    Y11(ct_s)=y(1,1)+s(ct_s)*(cgsims1+cgdims1);
    Y12(ct_s)=y(1,2)-s(ct_s)*cgdims1;
    Y21(ct_s)=y(2,1)-s(ct_s)*cgdims1;
    Y22(ct_s)=y(2,2)+s(ct_s)*(cdsims+cgdims1);
    zel=y2z(y);
    Z11(ct_s)=zel(1,1);
    Z12(ct_s)=zel(1,2);
    Z21(ct_s)=zel(2,1);
    Z22(ct_s)=zel(2,2);
    s=y2s(y);
    S11(ct_s)=s(1,1);
    S12(ct_s)=s(1,2);
    S21(ct_s)=s(2,1);
    S22(ct_s)=s(2,2);
end

