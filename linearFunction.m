function ydata = linearFunction(para,xdata)

ydata = xdata.*para.m + para.c;

end