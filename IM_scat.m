function Imet = IM_scat(biasdata,PARA)

vgsdata=biasdata(:,1);
vdsdata=biasdata(:,2);
rmta=PARA.rmta;
amto=PARA.amto;
rscm=PARA.rscm;
rdcm=PARA.rdcm;

t202 = abs(vdsdata);
t205 = amto.*t202;
t203 = rdcm+rmta+rscm-t205;
t204 = 1.0./amto;
t206 = t203.*t204.*(1.0./2.0);
t207 = 1.0./amto.^2;
t208 = t203.^2;
t209 = t207.*t208.*(1.0./4.0);
t210 = rmta.*t202.*t204;
t211 = t209+t210;
t212 = sqrt(t211);
t213 = t206-t212;
Imet = -(t213.*(heaviside(vdsdata).*2.0-1.0))./(rmta-amto.*t213);
